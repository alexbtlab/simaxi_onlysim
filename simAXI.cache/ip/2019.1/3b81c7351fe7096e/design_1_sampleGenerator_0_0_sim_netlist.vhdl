-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Jun  6 00:53:17 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_sampleGenerator_0_0_sim_netlist.vhdl
-- Design      : design_1_sampleGenerator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  port (
    M_AXIS_TLAST : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en_AXIclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^m_axis_tlast\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_3\ : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_1_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_2_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_3_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_4_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_1 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_2 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_3 : STD_LOGIC;
  signal M_AXIS_TVALID_reg_i_1_n_0 : STD_LOGIC;
  signal wordCounter : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal wordCounter0 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \wordCounter[0]__0_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[0]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[10]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[11]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[12]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[13]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[14]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[15]__0_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[15]__0_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter[15]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[15]_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter[1]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[2]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[3]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[4]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[5]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[6]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[7]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[8]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[9]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[12]__0_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[12]__0_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[12]__0_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[12]__0_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[15]__0_i_3_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[15]__0_i_3_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[4]__0_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[4]__0_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[4]__0_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[4]__0_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[8]__0_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[8]__0_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[8]__0_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[8]__0_i_1_n_3\ : STD_LOGIC;
  signal NLW_M_AXIS_TLAST_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TLAST_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wordCounter_reg[15]__0_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_wordCounter_reg[15]__0_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \wordCounter[10]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \wordCounter[11]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \wordCounter[12]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \wordCounter[13]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \wordCounter[14]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \wordCounter[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \wordCounter[2]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \wordCounter[3]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \wordCounter[4]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \wordCounter[5]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \wordCounter[6]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \wordCounter[7]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \wordCounter[8]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \wordCounter[9]_i_1\ : label is "soft_lutpair4";
begin
  E(0) <= \^e\(0);
  M_AXIS_TLAST <= \^m_axis_tlast\;
\M_AXIS_TDATA_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(0),
      Q => m00_axis_tdata(0),
      R => '0'
    );
\M_AXIS_TDATA_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(10),
      Q => m00_axis_tdata(10),
      R => '0'
    );
\M_AXIS_TDATA_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(11),
      Q => m00_axis_tdata(11),
      R => '0'
    );
\M_AXIS_TDATA_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(12),
      Q => m00_axis_tdata(12),
      R => '0'
    );
\M_AXIS_TDATA_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(13),
      Q => m00_axis_tdata(13),
      R => '0'
    );
\M_AXIS_TDATA_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(14),
      Q => m00_axis_tdata(14),
      R => '0'
    );
\M_AXIS_TDATA_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(15),
      Q => m00_axis_tdata(15),
      R => '0'
    );
\M_AXIS_TDATA_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(16),
      Q => m00_axis_tdata(16),
      R => '0'
    );
\M_AXIS_TDATA_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(17),
      Q => m00_axis_tdata(17),
      R => '0'
    );
\M_AXIS_TDATA_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(18),
      Q => m00_axis_tdata(18),
      R => '0'
    );
\M_AXIS_TDATA_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(19),
      Q => m00_axis_tdata(19),
      R => '0'
    );
\M_AXIS_TDATA_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(1),
      Q => m00_axis_tdata(1),
      R => '0'
    );
\M_AXIS_TDATA_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(20),
      Q => m00_axis_tdata(20),
      R => '0'
    );
\M_AXIS_TDATA_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(21),
      Q => m00_axis_tdata(21),
      R => '0'
    );
\M_AXIS_TDATA_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(22),
      Q => m00_axis_tdata(22),
      R => '0'
    );
\M_AXIS_TDATA_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(23),
      Q => m00_axis_tdata(23),
      R => '0'
    );
\M_AXIS_TDATA_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(24),
      Q => m00_axis_tdata(24),
      R => '0'
    );
\M_AXIS_TDATA_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(25),
      Q => m00_axis_tdata(25),
      R => '0'
    );
\M_AXIS_TDATA_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(26),
      Q => m00_axis_tdata(26),
      R => '0'
    );
\M_AXIS_TDATA_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(27),
      Q => m00_axis_tdata(27),
      R => '0'
    );
\M_AXIS_TDATA_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(28),
      Q => m00_axis_tdata(28),
      R => '0'
    );
\M_AXIS_TDATA_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(29),
      Q => m00_axis_tdata(29),
      R => '0'
    );
\M_AXIS_TDATA_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(2),
      Q => m00_axis_tdata(2),
      R => '0'
    );
\M_AXIS_TDATA_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(30),
      Q => m00_axis_tdata(30),
      R => '0'
    );
\M_AXIS_TDATA_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(31),
      Q => m00_axis_tdata(31),
      R => '0'
    );
\M_AXIS_TDATA_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(3),
      Q => m00_axis_tdata(3),
      R => '0'
    );
\M_AXIS_TDATA_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(4),
      Q => m00_axis_tdata(4),
      R => '0'
    );
\M_AXIS_TDATA_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(5),
      Q => m00_axis_tdata(5),
      R => '0'
    );
\M_AXIS_TDATA_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(6),
      Q => m00_axis_tdata(6),
      R => '0'
    );
\M_AXIS_TDATA_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(7),
      Q => m00_axis_tdata(7),
      R => '0'
    );
\M_AXIS_TDATA_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(8),
      Q => m00_axis_tdata(8),
      R => '0'
    );
\M_AXIS_TDATA_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => m00_axis_aresetn,
      D => data_from_ram(9),
      Q => m00_axis_tdata(9),
      R => '0'
    );
M_AXIS_TLAST_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => M_AXIS_TLAST_carry_n_0,
      CO(2) => M_AXIS_TLAST_carry_n_1,
      CO(1) => M_AXIS_TLAST_carry_n_2,
      CO(0) => M_AXIS_TLAST_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_M_AXIS_TLAST_carry_O_UNCONNECTED(3 downto 0),
      S(3) => M_AXIS_TLAST_carry_i_1_n_0,
      S(2) => M_AXIS_TLAST_carry_i_2_n_0,
      S(1) => M_AXIS_TLAST_carry_i_3_n_0,
      S(0) => M_AXIS_TLAST_carry_i_4_n_0
    );
\M_AXIS_TLAST_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => M_AXIS_TLAST_carry_n_0,
      CO(3 downto 2) => \NLW_M_AXIS_TLAST_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \^m_axis_tlast\,
      CO(0) => \M_AXIS_TLAST_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \M_AXIS_TLAST_carry__0_i_1_n_0\,
      S(0) => \M_AXIS_TLAST_carry__0_i_2_n_0\
    );
\M_AXIS_TLAST_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wordCounter(15),
      O => \M_AXIS_TLAST_carry__0_i_1_n_0\
    );
\M_AXIS_TLAST_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => wordCounter(14),
      I1 => wordCounter(13),
      I2 => wordCounter(12),
      O => \M_AXIS_TLAST_carry__0_i_2_n_0\
    );
M_AXIS_TLAST_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => wordCounter(11),
      I1 => wordCounter(10),
      I2 => wordCounter(9),
      O => M_AXIS_TLAST_carry_i_1_n_0
    );
M_AXIS_TLAST_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => wordCounter(6),
      I1 => wordCounter(8),
      I2 => wordCounter(7),
      O => M_AXIS_TLAST_carry_i_2_n_0
    );
M_AXIS_TLAST_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => wordCounter(3),
      I1 => wordCounter(5),
      I2 => wordCounter(4),
      O => M_AXIS_TLAST_carry_i_3_n_0
    );
M_AXIS_TLAST_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => wordCounter(2),
      I1 => wordCounter(1),
      I2 => wordCounter(0),
      O => M_AXIS_TLAST_carry_i_4_n_0
    );
M_AXIS_TVALID_reg_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => en_AXIclk,
      O => M_AXIS_TVALID_reg_i_1_n_0
    );
M_AXIS_TVALID_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => M_AXIS_TVALID_reg_i_1_n_0,
      Q => \^e\(0),
      R => '0'
    );
\wordCounter[0]__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wordCounter(0),
      O => \wordCounter[0]__0_i_1_n_0\
    );
\wordCounter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^m_axis_tlast\,
      I1 => wordCounter(0),
      O => \wordCounter[0]_i_1_n_0\
    );
\wordCounter[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(10),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[10]_i_1_n_0\
    );
\wordCounter[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(11),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[11]_i_1_n_0\
    );
\wordCounter[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(12),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[12]_i_1_n_0\
    );
\wordCounter[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(13),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[13]_i_1_n_0\
    );
\wordCounter[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(14),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[14]_i_1_n_0\
    );
\wordCounter[15]__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \^m_axis_tlast\,
      I1 => en_AXIclk,
      I2 => m00_axis_aresetn,
      O => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter[15]__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => en_AXIclk,
      O => \wordCounter[15]__0_i_2_n_0\
    );
\wordCounter[15]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => m00_axis_aresetn,
      O => \wordCounter[15]_i_1_n_0\
    );
\wordCounter[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(15),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[15]_i_2_n_0\
    );
\wordCounter[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(1),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[1]_i_1_n_0\
    );
\wordCounter[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(2),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[2]_i_1_n_0\
    );
\wordCounter[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(3),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[3]_i_1_n_0\
    );
\wordCounter[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(4),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[4]_i_1_n_0\
    );
\wordCounter[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(5),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[5]_i_1_n_0\
    );
\wordCounter[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(6),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[6]_i_1_n_0\
    );
\wordCounter[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(7),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[7]_i_1_n_0\
    );
\wordCounter[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(8),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[8]_i_1_n_0\
    );
\wordCounter[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wordCounter0(9),
      I1 => \^m_axis_tlast\,
      O => \wordCounter[9]_i_1_n_0\
    );
\wordCounter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[0]_i_1_n_0\,
      Q => wordCounter(0),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[0]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => \wordCounter[0]__0_i_1_n_0\,
      Q => wordCounter(0),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[10]_i_1_n_0\,
      Q => wordCounter(10),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[10]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(10),
      Q => wordCounter(10),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[11]_i_1_n_0\,
      Q => wordCounter(11),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[11]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(11),
      Q => wordCounter(11),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[12]_i_1_n_0\,
      Q => wordCounter(12),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[12]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(12),
      Q => wordCounter(12),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[12]__0_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[8]__0_i_1_n_0\,
      CO(3) => \wordCounter_reg[12]__0_i_1_n_0\,
      CO(2) => \wordCounter_reg[12]__0_i_1_n_1\,
      CO(1) => \wordCounter_reg[12]__0_i_1_n_2\,
      CO(0) => \wordCounter_reg[12]__0_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => wordCounter0(12 downto 9),
      S(3 downto 0) => wordCounter(12 downto 9)
    );
\wordCounter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[13]_i_1_n_0\,
      Q => wordCounter(13),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[13]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(13),
      Q => wordCounter(13),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[14]_i_1_n_0\,
      Q => wordCounter(14),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[14]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(14),
      Q => wordCounter(14),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[15]_i_2_n_0\,
      Q => wordCounter(15),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[15]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(15),
      Q => wordCounter(15),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[15]__0_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[12]__0_i_1_n_0\,
      CO(3 downto 2) => \NLW_wordCounter_reg[15]__0_i_3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \wordCounter_reg[15]__0_i_3_n_2\,
      CO(0) => \wordCounter_reg[15]__0_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_wordCounter_reg[15]__0_i_3_O_UNCONNECTED\(3),
      O(2 downto 0) => wordCounter0(15 downto 13),
      S(3) => '0',
      S(2 downto 0) => wordCounter(15 downto 13)
    );
\wordCounter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[1]_i_1_n_0\,
      Q => wordCounter(1),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[1]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(1),
      Q => wordCounter(1),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[2]_i_1_n_0\,
      Q => wordCounter(2),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[2]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(2),
      Q => wordCounter(2),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[3]_i_1_n_0\,
      Q => wordCounter(3),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[3]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(3),
      Q => wordCounter(3),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[4]_i_1_n_0\,
      Q => wordCounter(4),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[4]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(4),
      Q => wordCounter(4),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[4]__0_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wordCounter_reg[4]__0_i_1_n_0\,
      CO(2) => \wordCounter_reg[4]__0_i_1_n_1\,
      CO(1) => \wordCounter_reg[4]__0_i_1_n_2\,
      CO(0) => \wordCounter_reg[4]__0_i_1_n_3\,
      CYINIT => wordCounter(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => wordCounter0(4 downto 1),
      S(3 downto 0) => wordCounter(4 downto 1)
    );
\wordCounter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[5]_i_1_n_0\,
      Q => wordCounter(5),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[5]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(5),
      Q => wordCounter(5),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[6]_i_1_n_0\,
      Q => wordCounter(6),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[6]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(6),
      Q => wordCounter(6),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[7]_i_1_n_0\,
      Q => wordCounter(7),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[7]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(7),
      Q => wordCounter(7),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[8]_i_1_n_0\,
      Q => wordCounter(8),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[8]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(8),
      Q => wordCounter(8),
      R => \wordCounter[15]__0_i_1_n_0\
    );
\wordCounter_reg[8]__0_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[4]__0_i_1_n_0\,
      CO(3) => \wordCounter_reg[8]__0_i_1_n_0\,
      CO(2) => \wordCounter_reg[8]__0_i_1_n_1\,
      CO(1) => \wordCounter_reg[8]__0_i_1_n_2\,
      CO(0) => \wordCounter_reg[8]__0_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => wordCounter0(8 downto 5),
      S(3 downto 0) => wordCounter(8 downto 5)
    );
\wordCounter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^e\(0),
      D => \wordCounter[9]_i_1_n_0\,
      Q => wordCounter(9),
      R => \wordCounter[15]_i_1_n_0\
    );
\wordCounter_reg[9]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[15]__0_i_2_n_0\,
      D => wordCounter0(9),
      Q => wordCounter(9),
      R => \wordCounter[15]__0_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
  port (
    m00_axis_tlast : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    en_AXIclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
begin
samplGeneraror_v1_0_M00_AXIS_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS
     port map (
      E(0) => E(0),
      M_AXIS_TLAST => m00_axis_tlast,
      data_from_ram(31 downto 0) => data_from_ram(31 downto 0),
      en_AXIclk => en_AXIclk,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    en_AXIclk : in STD_LOGIC;
    clk_read_ram : out STD_LOGIC;
    adr_read_ram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_sampleGenerator_0_0,sampleGenerator_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sampleGenerator_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
  clk_read_ram <= 'Z';
  adr_read_ram(0) <= 'Z';
  adr_read_ram(1) <= 'Z';
  adr_read_ram(2) <= 'Z';
  adr_read_ram(3) <= 'Z';
  adr_read_ram(4) <= 'Z';
  adr_read_ram(5) <= 'Z';
  adr_read_ram(6) <= 'Z';
  adr_read_ram(7) <= 'Z';
  adr_read_ram(8) <= 'Z';
  adr_read_ram(9) <= 'Z';
  adr_read_ram(10) <= 'Z';
  adr_read_ram(11) <= 'Z';
  adr_read_ram(12) <= 'Z';
  adr_read_ram(13) <= 'Z';
  adr_read_ram(14) <= 'Z';
  adr_read_ram(15) <= 'Z';
  adr_read_ram(16) <= 'Z';
  adr_read_ram(17) <= 'Z';
  adr_read_ram(18) <= 'Z';
  adr_read_ram(19) <= 'Z';
  adr_read_ram(20) <= 'Z';
  adr_read_ram(21) <= 'Z';
  adr_read_ram(22) <= 'Z';
  adr_read_ram(23) <= 'Z';
  adr_read_ram(24) <= 'Z';
  adr_read_ram(25) <= 'Z';
  adr_read_ram(26) <= 'Z';
  adr_read_ram(27) <= 'Z';
  adr_read_ram(28) <= 'Z';
  adr_read_ram(29) <= 'Z';
  adr_read_ram(30) <= 'Z';
  adr_read_ram(31) <= 'Z';
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0
     port map (
      E(0) => m00_axis_tvalid,
      data_from_ram(31 downto 0) => data_from_ram(31 downto 0),
      en_AXIclk => en_AXIclk,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast
    );
end STRUCTURE;
