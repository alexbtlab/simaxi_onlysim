-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Thu Jun  4 22:14:55 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_imitation_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  port (
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_100MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  signal FLAG_state : STD_LOGIC;
  signal FLAG_state0_out : STD_LOGIC;
  signal \FLAG_state__0_i_1_n_0\ : STD_LOGIC;
  signal FLAG_state_i_1_n_0 : STD_LOGIC;
  signal \^adr_bram\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal count_clk1000 : STD_LOGIC;
  signal \count_clk100[0]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_10_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_11_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_12_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_5_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_6_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_7_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_8_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_9_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal count_clk5 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \count_clk5[31]_i_10_n_0\ : STD_LOGIC;
  signal \count_clk5[31]_i_3_n_0\ : STD_LOGIC;
  signal \count_clk5[31]_i_4_n_0\ : STD_LOGIC;
  signal \count_clk5[31]_i_5_n_0\ : STD_LOGIC;
  signal \count_clk5[31]_i_6_n_0\ : STD_LOGIC;
  signal \count_clk5[31]_i_7_n_0\ : STD_LOGIC;
  signal \count_clk5[31]_i_8_n_0\ : STD_LOGIC;
  signal \count_clk5[31]_i_9_n_0\ : STD_LOGIC;
  signal count_clk5_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \count_clk5_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal data_adc_deserial0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal \NLW_count_clk100_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_clk100_reg[31]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_count_clk5_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_clk5_reg[31]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count_clk5[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count_clk5[31]_i_5\ : label is "soft_lutpair0";
begin
  adr_bram(31 downto 0) <= \^adr_bram\(31 downto 0);
\FLAG_state__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \count_clk5[31]_i_6_n_0\,
      I1 => \count_clk5[31]_i_5_n_0\,
      I2 => \count_clk5[31]_i_4_n_0\,
      I3 => \count_clk5[31]_i_3_n_0\,
      I4 => FLAG_state,
      O => \FLAG_state__0_i_1_n_0\
    );
FLAG_state_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \count_clk100[31]_i_8_n_0\,
      I1 => \count_clk100[31]_i_7_n_0\,
      I2 => \count_clk100[31]_i_6_n_0\,
      I3 => \count_clk100[31]_i_5_n_0\,
      I4 => FLAG_state,
      O => FLAG_state_i_1_n_0
    );
FLAG_state_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => '1',
      D => FLAG_state_i_1_n_0,
      Q => FLAG_state,
      R => '0'
    );
\FLAG_state_reg__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \FLAG_state__0_i_1_n_0\,
      Q => FLAG_state,
      R => '0'
    );
\count_clk100[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => FLAG_state,
      I1 => \^adr_bram\(0),
      O => \count_clk100[0]_i_1_n_0\
    );
\count_clk100[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \count_clk100[31]_i_5_n_0\,
      I1 => \count_clk100[31]_i_6_n_0\,
      I2 => \count_clk100[31]_i_7_n_0\,
      I3 => \count_clk100[31]_i_8_n_0\,
      I4 => FLAG_state,
      O => \count_clk100[31]_i_1_n_0\
    );
\count_clk100[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^adr_bram\(29),
      I1 => \^adr_bram\(28),
      I2 => \^adr_bram\(31),
      I3 => \^adr_bram\(30),
      O => \count_clk100[31]_i_10_n_0\
    );
\count_clk100[31]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \^adr_bram\(5),
      I1 => \^adr_bram\(4),
      I2 => \^adr_bram\(7),
      I3 => \^adr_bram\(6),
      O => \count_clk100[31]_i_11_n_0\
    );
\count_clk100[31]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \^adr_bram\(12),
      I1 => \^adr_bram\(13),
      I2 => \^adr_bram\(15),
      I3 => \^adr_bram\(14),
      O => \count_clk100[31]_i_12_n_0\
    );
\count_clk100[31]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FLAG_state,
      O => p_2_in
    );
\count_clk100[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_100MHz,
      I1 => s00_axi_aresetn,
      O => count_clk1000
    );
\count_clk100[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^adr_bram\(18),
      I1 => \^adr_bram\(19),
      I2 => \^adr_bram\(16),
      I3 => \^adr_bram\(17),
      I4 => \count_clk100[31]_i_9_n_0\,
      O => \count_clk100[31]_i_5_n_0\
    );
\count_clk100[31]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^adr_bram\(26),
      I1 => \^adr_bram\(27),
      I2 => \^adr_bram\(24),
      I3 => \^adr_bram\(25),
      I4 => \count_clk100[31]_i_10_n_0\,
      O => \count_clk100[31]_i_6_n_0\
    );
\count_clk100[31]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \^adr_bram\(2),
      I1 => \^adr_bram\(3),
      I2 => \^adr_bram\(0),
      I3 => \^adr_bram\(1),
      I4 => \count_clk100[31]_i_11_n_0\,
      O => \count_clk100[31]_i_7_n_0\
    );
\count_clk100[31]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \^adr_bram\(10),
      I1 => \^adr_bram\(11),
      I2 => \^adr_bram\(8),
      I3 => \^adr_bram\(9),
      I4 => \count_clk100[31]_i_12_n_0\,
      O => \count_clk100[31]_i_8_n_0\
    );
\count_clk100[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^adr_bram\(21),
      I1 => \^adr_bram\(20),
      I2 => \^adr_bram\(23),
      I3 => \^adr_bram\(22),
      O => \count_clk100[31]_i_9_n_0\
    );
\count_clk100_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => '1',
      D => \count_clk100[0]_i_1_n_0\,
      Q => \^adr_bram\(0),
      R => '0'
    );
\count_clk100_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(10),
      Q => \^adr_bram\(10),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(11),
      Q => \^adr_bram\(11),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(12),
      Q => \^adr_bram\(12),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[8]_i_1_n_0\,
      CO(3) => \count_clk100_reg[12]_i_1_n_0\,
      CO(2) => \count_clk100_reg[12]_i_1_n_1\,
      CO(1) => \count_clk100_reg[12]_i_1_n_2\,
      CO(0) => \count_clk100_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => \^adr_bram\(12 downto 9)
    );
\count_clk100_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(13),
      Q => \^adr_bram\(13),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(14),
      Q => \^adr_bram\(14),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(15),
      Q => \^adr_bram\(15),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(16),
      Q => \^adr_bram\(16),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[12]_i_1_n_0\,
      CO(3) => \count_clk100_reg[16]_i_1_n_0\,
      CO(2) => \count_clk100_reg[16]_i_1_n_1\,
      CO(1) => \count_clk100_reg[16]_i_1_n_2\,
      CO(0) => \count_clk100_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3 downto 0) => \^adr_bram\(16 downto 13)
    );
\count_clk100_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(17),
      Q => \^adr_bram\(17),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(18),
      Q => \^adr_bram\(18),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(19),
      Q => \^adr_bram\(19),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(1),
      Q => \^adr_bram\(1),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(20),
      Q => \^adr_bram\(20),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[16]_i_1_n_0\,
      CO(3) => \count_clk100_reg[20]_i_1_n_0\,
      CO(2) => \count_clk100_reg[20]_i_1_n_1\,
      CO(1) => \count_clk100_reg[20]_i_1_n_2\,
      CO(0) => \count_clk100_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(20 downto 17),
      S(3 downto 0) => \^adr_bram\(20 downto 17)
    );
\count_clk100_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(21),
      Q => \^adr_bram\(21),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(22),
      Q => \^adr_bram\(22),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(23),
      Q => \^adr_bram\(23),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(24),
      Q => \^adr_bram\(24),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[20]_i_1_n_0\,
      CO(3) => \count_clk100_reg[24]_i_1_n_0\,
      CO(2) => \count_clk100_reg[24]_i_1_n_1\,
      CO(1) => \count_clk100_reg[24]_i_1_n_2\,
      CO(0) => \count_clk100_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(24 downto 21),
      S(3 downto 0) => \^adr_bram\(24 downto 21)
    );
\count_clk100_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(25),
      Q => \^adr_bram\(25),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(26),
      Q => \^adr_bram\(26),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(27),
      Q => \^adr_bram\(27),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(28),
      Q => \^adr_bram\(28),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[24]_i_1_n_0\,
      CO(3) => \count_clk100_reg[28]_i_1_n_0\,
      CO(2) => \count_clk100_reg[28]_i_1_n_1\,
      CO(1) => \count_clk100_reg[28]_i_1_n_2\,
      CO(0) => \count_clk100_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(28 downto 25),
      S(3 downto 0) => \^adr_bram\(28 downto 25)
    );
\count_clk100_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(29),
      Q => \^adr_bram\(29),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(2),
      Q => \^adr_bram\(2),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(30),
      Q => \^adr_bram\(30),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(31),
      Q => \^adr_bram\(31),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[28]_i_1_n_0\,
      CO(3 downto 2) => \NLW_count_clk100_reg[31]_i_3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count_clk100_reg[31]_i_3_n_2\,
      CO(0) => \count_clk100_reg[31]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_count_clk100_reg[31]_i_3_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => \^adr_bram\(31 downto 29)
    );
\count_clk100_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(3),
      Q => \^adr_bram\(3),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(4),
      Q => \^adr_bram\(4),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_clk100_reg[4]_i_1_n_0\,
      CO(2) => \count_clk100_reg[4]_i_1_n_1\,
      CO(1) => \count_clk100_reg[4]_i_1_n_2\,
      CO(0) => \count_clk100_reg[4]_i_1_n_3\,
      CYINIT => \^adr_bram\(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => \^adr_bram\(4 downto 1)
    );
\count_clk100_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(5),
      Q => \^adr_bram\(5),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(6),
      Q => \^adr_bram\(6),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(7),
      Q => \^adr_bram\(7),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(8),
      Q => \^adr_bram\(8),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[4]_i_1_n_0\,
      CO(3) => \count_clk100_reg[8]_i_1_n_0\,
      CO(2) => \count_clk100_reg[8]_i_1_n_1\,
      CO(1) => \count_clk100_reg[8]_i_1_n_2\,
      CO(0) => \count_clk100_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => \^adr_bram\(8 downto 5)
    );
\count_clk100_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_2_in,
      D => data0(9),
      Q => \^adr_bram\(9),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk5[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_clk5(0),
      O => count_clk5_0(0)
    );
\count_clk5[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_5MHz,
      I1 => s00_axi_aresetn,
      O => data_adc_deserial0
    );
\count_clk5[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => FLAG_state,
      I1 => \count_clk5[31]_i_3_n_0\,
      I2 => \count_clk5[31]_i_4_n_0\,
      I3 => \count_clk5[31]_i_5_n_0\,
      I4 => \count_clk5[31]_i_6_n_0\,
      O => FLAG_state0_out
    );
\count_clk5[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_clk5(12),
      I1 => count_clk5(13),
      I2 => count_clk5(15),
      I3 => count_clk5(14),
      O => \count_clk5[31]_i_10_n_0\
    );
\count_clk5[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => count_clk5(18),
      I1 => count_clk5(19),
      I2 => count_clk5(16),
      I3 => count_clk5(17),
      I4 => \count_clk5[31]_i_7_n_0\,
      O => \count_clk5[31]_i_3_n_0\
    );
\count_clk5[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => count_clk5(26),
      I1 => count_clk5(27),
      I2 => count_clk5(24),
      I3 => count_clk5(25),
      I4 => \count_clk5[31]_i_8_n_0\,
      O => \count_clk5[31]_i_4_n_0\
    );
\count_clk5[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => count_clk5(2),
      I1 => count_clk5(3),
      I2 => count_clk5(0),
      I3 => count_clk5(1),
      I4 => \count_clk5[31]_i_9_n_0\,
      O => \count_clk5[31]_i_5_n_0\
    );
\count_clk5[31]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => count_clk5(10),
      I1 => count_clk5(11),
      I2 => count_clk5(8),
      I3 => count_clk5(9),
      I4 => \count_clk5[31]_i_10_n_0\,
      O => \count_clk5[31]_i_6_n_0\
    );
\count_clk5[31]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk5(21),
      I1 => count_clk5(20),
      I2 => count_clk5(23),
      I3 => count_clk5(22),
      O => \count_clk5[31]_i_7_n_0\
    );
\count_clk5[31]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk5(29),
      I1 => count_clk5(28),
      I2 => count_clk5(31),
      I3 => count_clk5(30),
      O => \count_clk5[31]_i_8_n_0\
    );
\count_clk5[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => count_clk5(5),
      I1 => count_clk5(4),
      I2 => count_clk5(7),
      I3 => count_clk5(6),
      O => \count_clk5[31]_i_9_n_0\
    );
\count_clk5_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => count_clk5_0(0),
      Q => count_clk5(0),
      R => '0'
    );
\count_clk5_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[12]_i_1_n_6\,
      Q => count_clk5(10),
      R => FLAG_state0_out
    );
\count_clk5_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[12]_i_1_n_5\,
      Q => count_clk5(11),
      R => FLAG_state0_out
    );
\count_clk5_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[12]_i_1_n_4\,
      Q => count_clk5(12),
      R => FLAG_state0_out
    );
\count_clk5_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[8]_i_1_n_0\,
      CO(3) => \count_clk5_reg[12]_i_1_n_0\,
      CO(2) => \count_clk5_reg[12]_i_1_n_1\,
      CO(1) => \count_clk5_reg[12]_i_1_n_2\,
      CO(0) => \count_clk5_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[12]_i_1_n_4\,
      O(2) => \count_clk5_reg[12]_i_1_n_5\,
      O(1) => \count_clk5_reg[12]_i_1_n_6\,
      O(0) => \count_clk5_reg[12]_i_1_n_7\,
      S(3 downto 0) => count_clk5(12 downto 9)
    );
\count_clk5_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[16]_i_1_n_7\,
      Q => count_clk5(13),
      R => FLAG_state0_out
    );
\count_clk5_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[16]_i_1_n_6\,
      Q => count_clk5(14),
      R => FLAG_state0_out
    );
\count_clk5_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[16]_i_1_n_5\,
      Q => count_clk5(15),
      R => FLAG_state0_out
    );
\count_clk5_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[16]_i_1_n_4\,
      Q => count_clk5(16),
      R => FLAG_state0_out
    );
\count_clk5_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[12]_i_1_n_0\,
      CO(3) => \count_clk5_reg[16]_i_1_n_0\,
      CO(2) => \count_clk5_reg[16]_i_1_n_1\,
      CO(1) => \count_clk5_reg[16]_i_1_n_2\,
      CO(0) => \count_clk5_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[16]_i_1_n_4\,
      O(2) => \count_clk5_reg[16]_i_1_n_5\,
      O(1) => \count_clk5_reg[16]_i_1_n_6\,
      O(0) => \count_clk5_reg[16]_i_1_n_7\,
      S(3 downto 0) => count_clk5(16 downto 13)
    );
\count_clk5_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[20]_i_1_n_7\,
      Q => count_clk5(17),
      R => FLAG_state0_out
    );
\count_clk5_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[20]_i_1_n_6\,
      Q => count_clk5(18),
      R => FLAG_state0_out
    );
\count_clk5_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[20]_i_1_n_5\,
      Q => count_clk5(19),
      R => FLAG_state0_out
    );
\count_clk5_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[4]_i_1_n_7\,
      Q => count_clk5(1),
      R => FLAG_state0_out
    );
\count_clk5_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[20]_i_1_n_4\,
      Q => count_clk5(20),
      R => FLAG_state0_out
    );
\count_clk5_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[16]_i_1_n_0\,
      CO(3) => \count_clk5_reg[20]_i_1_n_0\,
      CO(2) => \count_clk5_reg[20]_i_1_n_1\,
      CO(1) => \count_clk5_reg[20]_i_1_n_2\,
      CO(0) => \count_clk5_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[20]_i_1_n_4\,
      O(2) => \count_clk5_reg[20]_i_1_n_5\,
      O(1) => \count_clk5_reg[20]_i_1_n_6\,
      O(0) => \count_clk5_reg[20]_i_1_n_7\,
      S(3 downto 0) => count_clk5(20 downto 17)
    );
\count_clk5_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[24]_i_1_n_7\,
      Q => count_clk5(21),
      R => FLAG_state0_out
    );
\count_clk5_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[24]_i_1_n_6\,
      Q => count_clk5(22),
      R => FLAG_state0_out
    );
\count_clk5_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[24]_i_1_n_5\,
      Q => count_clk5(23),
      R => FLAG_state0_out
    );
\count_clk5_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[24]_i_1_n_4\,
      Q => count_clk5(24),
      R => FLAG_state0_out
    );
\count_clk5_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[20]_i_1_n_0\,
      CO(3) => \count_clk5_reg[24]_i_1_n_0\,
      CO(2) => \count_clk5_reg[24]_i_1_n_1\,
      CO(1) => \count_clk5_reg[24]_i_1_n_2\,
      CO(0) => \count_clk5_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[24]_i_1_n_4\,
      O(2) => \count_clk5_reg[24]_i_1_n_5\,
      O(1) => \count_clk5_reg[24]_i_1_n_6\,
      O(0) => \count_clk5_reg[24]_i_1_n_7\,
      S(3 downto 0) => count_clk5(24 downto 21)
    );
\count_clk5_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[28]_i_1_n_7\,
      Q => count_clk5(25),
      R => FLAG_state0_out
    );
\count_clk5_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[28]_i_1_n_6\,
      Q => count_clk5(26),
      R => FLAG_state0_out
    );
\count_clk5_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[28]_i_1_n_5\,
      Q => count_clk5(27),
      R => FLAG_state0_out
    );
\count_clk5_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[28]_i_1_n_4\,
      Q => count_clk5(28),
      R => FLAG_state0_out
    );
\count_clk5_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[24]_i_1_n_0\,
      CO(3) => \count_clk5_reg[28]_i_1_n_0\,
      CO(2) => \count_clk5_reg[28]_i_1_n_1\,
      CO(1) => \count_clk5_reg[28]_i_1_n_2\,
      CO(0) => \count_clk5_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[28]_i_1_n_4\,
      O(2) => \count_clk5_reg[28]_i_1_n_5\,
      O(1) => \count_clk5_reg[28]_i_1_n_6\,
      O(0) => \count_clk5_reg[28]_i_1_n_7\,
      S(3 downto 0) => count_clk5(28 downto 25)
    );
\count_clk5_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[31]_i_2_n_7\,
      Q => count_clk5(29),
      R => FLAG_state0_out
    );
\count_clk5_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[4]_i_1_n_6\,
      Q => count_clk5(2),
      R => FLAG_state0_out
    );
\count_clk5_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[31]_i_2_n_6\,
      Q => count_clk5(30),
      R => FLAG_state0_out
    );
\count_clk5_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[31]_i_2_n_5\,
      Q => count_clk5(31),
      R => FLAG_state0_out
    );
\count_clk5_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[28]_i_1_n_0\,
      CO(3 downto 2) => \NLW_count_clk5_reg[31]_i_2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count_clk5_reg[31]_i_2_n_2\,
      CO(0) => \count_clk5_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_count_clk5_reg[31]_i_2_O_UNCONNECTED\(3),
      O(2) => \count_clk5_reg[31]_i_2_n_5\,
      O(1) => \count_clk5_reg[31]_i_2_n_6\,
      O(0) => \count_clk5_reg[31]_i_2_n_7\,
      S(3) => '0',
      S(2 downto 0) => count_clk5(31 downto 29)
    );
\count_clk5_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[4]_i_1_n_5\,
      Q => count_clk5(3),
      R => FLAG_state0_out
    );
\count_clk5_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[4]_i_1_n_4\,
      Q => count_clk5(4),
      R => FLAG_state0_out
    );
\count_clk5_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_clk5_reg[4]_i_1_n_0\,
      CO(2) => \count_clk5_reg[4]_i_1_n_1\,
      CO(1) => \count_clk5_reg[4]_i_1_n_2\,
      CO(0) => \count_clk5_reg[4]_i_1_n_3\,
      CYINIT => count_clk5(0),
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[4]_i_1_n_4\,
      O(2) => \count_clk5_reg[4]_i_1_n_5\,
      O(1) => \count_clk5_reg[4]_i_1_n_6\,
      O(0) => \count_clk5_reg[4]_i_1_n_7\,
      S(3 downto 0) => count_clk5(4 downto 1)
    );
\count_clk5_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[8]_i_1_n_7\,
      Q => count_clk5(5),
      R => FLAG_state0_out
    );
\count_clk5_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[8]_i_1_n_6\,
      Q => count_clk5(6),
      R => FLAG_state0_out
    );
\count_clk5_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[8]_i_1_n_5\,
      Q => count_clk5(7),
      R => FLAG_state0_out
    );
\count_clk5_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[8]_i_1_n_4\,
      Q => count_clk5(8),
      R => FLAG_state0_out
    );
\count_clk5_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[4]_i_1_n_0\,
      CO(3) => \count_clk5_reg[8]_i_1_n_0\,
      CO(2) => \count_clk5_reg[8]_i_1_n_1\,
      CO(1) => \count_clk5_reg[8]_i_1_n_2\,
      CO(0) => \count_clk5_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[8]_i_1_n_4\,
      O(2) => \count_clk5_reg[8]_i_1_n_5\,
      O(1) => \count_clk5_reg[8]_i_1_n_6\,
      O(0) => \count_clk5_reg[8]_i_1_n_7\,
      S(3 downto 0) => count_clk5(8 downto 5)
    );
\count_clk5_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => FLAG_state,
      D => \count_clk5_reg[12]_i_1_n_7\,
      Q => count_clk5(9),
      R => FLAG_state0_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_5MHz : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    last_word_transmit : in STD_LOGIC;
    adr_read_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_5MHzWR_100MhzRD : out STD_LOGIC;
    clk_100Mhz_AXI_send : out STD_LOGIC;
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    write : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_imitation_0_0,ADC_imitation,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_imitation,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^clk_100mhz\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  \^clk_100mhz\ <= clk_100MHz;
  clk_5MHzWR_100MhzRD <= \^clk_100mhz\;
  data(31) <= \<const0>\;
  data(30) <= \<const0>\;
  data(29) <= \<const0>\;
  data(28) <= \<const0>\;
  data(27) <= \<const0>\;
  data(26) <= \<const0>\;
  data(25) <= \<const0>\;
  data(24) <= \<const0>\;
  data(23) <= \<const0>\;
  data(22) <= \<const0>\;
  data(21) <= \<const0>\;
  data(20) <= \<const0>\;
  data(19) <= \<const0>\;
  data(18) <= \<const0>\;
  data(17) <= \<const0>\;
  data(16) <= \<const0>\;
  data(15) <= \<const0>\;
  data(14) <= \<const0>\;
  data(13) <= \<const0>\;
  data(12) <= \<const0>\;
  data(11) <= \<const0>\;
  data(10) <= \<const0>\;
  data(9) <= \<const0>\;
  data(8) <= \<const0>\;
  data(7) <= \<const0>\;
  data(6) <= \<const0>\;
  data(5) <= \<const0>\;
  data(4) <= \<const0>\;
  data(3) <= \<const0>\;
  data(2) <= \<const0>\;
  data(1) <= \<const0>\;
  data(0) <= \<const0>\;
  write <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
clk_100Mhz_AXI_send_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^clk_100mhz\,
      O => clk_100Mhz_AXI_send
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
     port map (
      adr_bram(31 downto 0) => adr_bram(31 downto 0),
      clk_100MHz => \^clk_100mhz\,
      clk_5MHz => clk_5MHz,
      s00_axi_aresetn => s00_axi_aresetn
    );
end STRUCTURE;
