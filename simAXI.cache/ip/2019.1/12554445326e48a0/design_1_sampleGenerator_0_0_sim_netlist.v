// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Jun  6 12:04:07 2020
// Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_sampleGenerator_0_0_sim_netlist.v
// Design      : design_1_sampleGenerator_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_sampleGenerator_0_0,sampleGenerator_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "sampleGenerator_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tready,
    data_from_ram,
    en_AXIclk,
    clk_read_ram,
    adr_read_ram,
    m00_axis_aclk,
    m00_axis_aresetn);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  input [31:0]data_from_ram;
  input en_AXIclk;
  output clk_read_ram;
  output [31:0]adr_read_ram;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;

  wire \<const1> ;
  wire [31:0]data_from_ram;
  wire en_AXIclk;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;

  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 inst
       (.data_from_ram(data_from_ram),
        .en_AXIclk(en_AXIclk),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS
   (m00_axis_tdata,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_aresetn,
    data_from_ram,
    m00_axis_aclk,
    en_AXIclk);
  output [31:0]m00_axis_tdata;
  output m00_axis_tlast;
  output m00_axis_tvalid;
  input m00_axis_aresetn;
  input [31:0]data_from_ram;
  input m00_axis_aclk;
  input en_AXIclk;

  wire M_AXIS_TLAST_carry__0_i_1_n_0;
  wire M_AXIS_TLAST_carry__0_i_2_n_0;
  wire M_AXIS_TLAST_carry__0_n_3;
  wire M_AXIS_TLAST_carry_i_1_n_0;
  wire M_AXIS_TLAST_carry_i_2_n_0;
  wire M_AXIS_TLAST_carry_i_3_n_0;
  wire M_AXIS_TLAST_carry_i_4_n_0;
  wire M_AXIS_TLAST_carry_n_0;
  wire M_AXIS_TLAST_carry_n_1;
  wire M_AXIS_TLAST_carry_n_2;
  wire M_AXIS_TLAST_carry_n_3;
  wire [31:0]data_from_ram;
  wire en_AXIclk;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;
  wire signal_valid_i_1_n_0;
  wire signal_valid_i_2_n_0;
  wire signal_valid_i_3_n_0;
  wire signal_valid_i_4_n_0;
  wire signal_valid_i_5_n_0;
  wire signal_valid_i_6_n_0;
  wire signal_valid_i_7_n_0;
  wire signal_valid_i_8_n_0;
  wire \wordCounter[0]_i_1_n_0 ;
  wire \wordCounter[0]_i_2_n_0 ;
  wire \wordCounter[0]_i_4_n_0 ;
  wire [15:0]wordCounter_reg;
  wire \wordCounter_reg[0]_i_3_n_0 ;
  wire \wordCounter_reg[0]_i_3_n_1 ;
  wire \wordCounter_reg[0]_i_3_n_2 ;
  wire \wordCounter_reg[0]_i_3_n_3 ;
  wire \wordCounter_reg[0]_i_3_n_4 ;
  wire \wordCounter_reg[0]_i_3_n_5 ;
  wire \wordCounter_reg[0]_i_3_n_6 ;
  wire \wordCounter_reg[0]_i_3_n_7 ;
  wire \wordCounter_reg[12]_i_1_n_1 ;
  wire \wordCounter_reg[12]_i_1_n_2 ;
  wire \wordCounter_reg[12]_i_1_n_3 ;
  wire \wordCounter_reg[12]_i_1_n_4 ;
  wire \wordCounter_reg[12]_i_1_n_5 ;
  wire \wordCounter_reg[12]_i_1_n_6 ;
  wire \wordCounter_reg[12]_i_1_n_7 ;
  wire \wordCounter_reg[4]_i_1_n_0 ;
  wire \wordCounter_reg[4]_i_1_n_1 ;
  wire \wordCounter_reg[4]_i_1_n_2 ;
  wire \wordCounter_reg[4]_i_1_n_3 ;
  wire \wordCounter_reg[4]_i_1_n_4 ;
  wire \wordCounter_reg[4]_i_1_n_5 ;
  wire \wordCounter_reg[4]_i_1_n_6 ;
  wire \wordCounter_reg[4]_i_1_n_7 ;
  wire \wordCounter_reg[8]_i_1_n_0 ;
  wire \wordCounter_reg[8]_i_1_n_1 ;
  wire \wordCounter_reg[8]_i_1_n_2 ;
  wire \wordCounter_reg[8]_i_1_n_3 ;
  wire \wordCounter_reg[8]_i_1_n_4 ;
  wire \wordCounter_reg[8]_i_1_n_5 ;
  wire \wordCounter_reg[8]_i_1_n_6 ;
  wire \wordCounter_reg[8]_i_1_n_7 ;
  wire [3:0]NLW_M_AXIS_TLAST_carry_O_UNCONNECTED;
  wire [3:2]NLW_M_AXIS_TLAST_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_wordCounter_reg[12]_i_1_CO_UNCONNECTED ;

  FDRE \M_AXIS_TDATA_reg[0] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[0]),
        .Q(m00_axis_tdata[0]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[10] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[10]),
        .Q(m00_axis_tdata[10]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[11] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[11]),
        .Q(m00_axis_tdata[11]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[12] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[12]),
        .Q(m00_axis_tdata[12]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[13] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[13]),
        .Q(m00_axis_tdata[13]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[14] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[14]),
        .Q(m00_axis_tdata[14]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[15] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[15]),
        .Q(m00_axis_tdata[15]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[16] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[16]),
        .Q(m00_axis_tdata[16]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[17] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[17]),
        .Q(m00_axis_tdata[17]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[18] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[18]),
        .Q(m00_axis_tdata[18]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[19] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[19]),
        .Q(m00_axis_tdata[19]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[1] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[1]),
        .Q(m00_axis_tdata[1]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[20] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[20]),
        .Q(m00_axis_tdata[20]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[21] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[21]),
        .Q(m00_axis_tdata[21]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[22] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[22]),
        .Q(m00_axis_tdata[22]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[23] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[23]),
        .Q(m00_axis_tdata[23]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[24] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[24]),
        .Q(m00_axis_tdata[24]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[25] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[25]),
        .Q(m00_axis_tdata[25]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[26] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[26]),
        .Q(m00_axis_tdata[26]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[27] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[27]),
        .Q(m00_axis_tdata[27]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[28] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[28]),
        .Q(m00_axis_tdata[28]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[29] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[29]),
        .Q(m00_axis_tdata[29]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[2] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[2]),
        .Q(m00_axis_tdata[2]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[30] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[30]),
        .Q(m00_axis_tdata[30]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[31] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[31]),
        .Q(m00_axis_tdata[31]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[3] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[3]),
        .Q(m00_axis_tdata[3]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[4] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[4]),
        .Q(m00_axis_tdata[4]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[5] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[5]),
        .Q(m00_axis_tdata[5]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[6] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[6]),
        .Q(m00_axis_tdata[6]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[7] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[7]),
        .Q(m00_axis_tdata[7]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[8] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[8]),
        .Q(m00_axis_tdata[8]),
        .R(1'b0));
  FDRE \M_AXIS_TDATA_reg[9] 
       (.C(m00_axis_aclk),
        .CE(m00_axis_aresetn),
        .D(data_from_ram[9]),
        .Q(m00_axis_tdata[9]),
        .R(1'b0));
  CARRY4 M_AXIS_TLAST_carry
       (.CI(1'b0),
        .CO({M_AXIS_TLAST_carry_n_0,M_AXIS_TLAST_carry_n_1,M_AXIS_TLAST_carry_n_2,M_AXIS_TLAST_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_M_AXIS_TLAST_carry_O_UNCONNECTED[3:0]),
        .S({M_AXIS_TLAST_carry_i_1_n_0,M_AXIS_TLAST_carry_i_2_n_0,M_AXIS_TLAST_carry_i_3_n_0,M_AXIS_TLAST_carry_i_4_n_0}));
  CARRY4 M_AXIS_TLAST_carry__0
       (.CI(M_AXIS_TLAST_carry_n_0),
        .CO({NLW_M_AXIS_TLAST_carry__0_CO_UNCONNECTED[3:2],m00_axis_tlast,M_AXIS_TLAST_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,M_AXIS_TLAST_carry__0_i_1_n_0,M_AXIS_TLAST_carry__0_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    M_AXIS_TLAST_carry__0_i_1
       (.I0(wordCounter_reg[15]),
        .O(M_AXIS_TLAST_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    M_AXIS_TLAST_carry__0_i_2
       (.I0(wordCounter_reg[13]),
        .I1(wordCounter_reg[14]),
        .I2(wordCounter_reg[12]),
        .O(M_AXIS_TLAST_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    M_AXIS_TLAST_carry_i_1
       (.I0(wordCounter_reg[9]),
        .I1(wordCounter_reg[10]),
        .I2(wordCounter_reg[11]),
        .O(M_AXIS_TLAST_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    M_AXIS_TLAST_carry_i_2
       (.I0(wordCounter_reg[8]),
        .I1(wordCounter_reg[6]),
        .I2(wordCounter_reg[7]),
        .O(M_AXIS_TLAST_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    M_AXIS_TLAST_carry_i_3
       (.I0(wordCounter_reg[3]),
        .I1(wordCounter_reg[4]),
        .I2(wordCounter_reg[5]),
        .O(M_AXIS_TLAST_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    M_AXIS_TLAST_carry_i_4
       (.I0(wordCounter_reg[0]),
        .I1(wordCounter_reg[1]),
        .I2(wordCounter_reg[2]),
        .O(M_AXIS_TLAST_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFFF2FF00000200)) 
    signal_valid_i_1
       (.I0(signal_valid_i_2_n_0),
        .I1(signal_valid_i_3_n_0),
        .I2(en_AXIclk),
        .I3(m00_axis_aresetn),
        .I4(m00_axis_tlast),
        .I5(m00_axis_tvalid),
        .O(signal_valid_i_1_n_0));
  LUT6 #(
    .INIT(64'h777FFFFFFFFFFFFF)) 
    signal_valid_i_2
       (.I0(signal_valid_i_4_n_0),
        .I1(wordCounter_reg[2]),
        .I2(wordCounter_reg[0]),
        .I3(wordCounter_reg[1]),
        .I4(wordCounter_reg[7]),
        .I5(wordCounter_reg[6]),
        .O(signal_valid_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF4)) 
    signal_valid_i_3
       (.I0(signal_valid_i_5_n_0),
        .I1(signal_valid_i_6_n_0),
        .I2(signal_valid_i_7_n_0),
        .I3(wordCounter_reg[8]),
        .I4(wordCounter_reg[9]),
        .I5(signal_valid_i_8_n_0),
        .O(signal_valid_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h80)) 
    signal_valid_i_4
       (.I0(wordCounter_reg[3]),
        .I1(wordCounter_reg[4]),
        .I2(wordCounter_reg[5]),
        .O(signal_valid_i_4_n_0));
  LUT4 #(
    .INIT(16'hFEEE)) 
    signal_valid_i_5
       (.I0(wordCounter_reg[7]),
        .I1(wordCounter_reg[5]),
        .I2(wordCounter_reg[0]),
        .I3(wordCounter_reg[1]),
        .O(signal_valid_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    signal_valid_i_6
       (.I0(wordCounter_reg[4]),
        .I1(wordCounter_reg[2]),
        .I2(wordCounter_reg[6]),
        .I3(wordCounter_reg[3]),
        .O(signal_valid_i_6_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    signal_valid_i_7
       (.I0(wordCounter_reg[11]),
        .I1(wordCounter_reg[10]),
        .O(signal_valid_i_7_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    signal_valid_i_8
       (.I0(wordCounter_reg[14]),
        .I1(wordCounter_reg[15]),
        .I2(wordCounter_reg[12]),
        .I3(wordCounter_reg[13]),
        .O(signal_valid_i_8_n_0));
  FDRE #(
    .INIT(1'b0)) 
    signal_valid_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(signal_valid_i_1_n_0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h5D)) 
    \wordCounter[0]_i_1 
       (.I0(m00_axis_aresetn),
        .I1(m00_axis_tlast),
        .I2(en_AXIclk),
        .O(\wordCounter[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wordCounter[0]_i_2 
       (.I0(en_AXIclk),
        .O(\wordCounter[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wordCounter[0]_i_4 
       (.I0(wordCounter_reg[0]),
        .O(\wordCounter[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[0]_i_3_n_7 ),
        .Q(wordCounter_reg[0]),
        .R(\wordCounter[0]_i_1_n_0 ));
  CARRY4 \wordCounter_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wordCounter_reg[0]_i_3_n_0 ,\wordCounter_reg[0]_i_3_n_1 ,\wordCounter_reg[0]_i_3_n_2 ,\wordCounter_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wordCounter_reg[0]_i_3_n_4 ,\wordCounter_reg[0]_i_3_n_5 ,\wordCounter_reg[0]_i_3_n_6 ,\wordCounter_reg[0]_i_3_n_7 }),
        .S({wordCounter_reg[3:1],\wordCounter[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[8]_i_1_n_5 ),
        .Q(wordCounter_reg[10]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[8]_i_1_n_4 ),
        .Q(wordCounter_reg[11]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[12] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[12]_i_1_n_7 ),
        .Q(wordCounter_reg[12]),
        .R(\wordCounter[0]_i_1_n_0 ));
  CARRY4 \wordCounter_reg[12]_i_1 
       (.CI(\wordCounter_reg[8]_i_1_n_0 ),
        .CO({\NLW_wordCounter_reg[12]_i_1_CO_UNCONNECTED [3],\wordCounter_reg[12]_i_1_n_1 ,\wordCounter_reg[12]_i_1_n_2 ,\wordCounter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wordCounter_reg[12]_i_1_n_4 ,\wordCounter_reg[12]_i_1_n_5 ,\wordCounter_reg[12]_i_1_n_6 ,\wordCounter_reg[12]_i_1_n_7 }),
        .S(wordCounter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[13] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[12]_i_1_n_6 ),
        .Q(wordCounter_reg[13]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[14] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[12]_i_1_n_5 ),
        .Q(wordCounter_reg[14]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[15] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[12]_i_1_n_4 ),
        .Q(wordCounter_reg[15]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[0]_i_3_n_6 ),
        .Q(wordCounter_reg[1]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[0]_i_3_n_5 ),
        .Q(wordCounter_reg[2]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[0]_i_3_n_4 ),
        .Q(wordCounter_reg[3]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[4]_i_1_n_7 ),
        .Q(wordCounter_reg[4]),
        .R(\wordCounter[0]_i_1_n_0 ));
  CARRY4 \wordCounter_reg[4]_i_1 
       (.CI(\wordCounter_reg[0]_i_3_n_0 ),
        .CO({\wordCounter_reg[4]_i_1_n_0 ,\wordCounter_reg[4]_i_1_n_1 ,\wordCounter_reg[4]_i_1_n_2 ,\wordCounter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wordCounter_reg[4]_i_1_n_4 ,\wordCounter_reg[4]_i_1_n_5 ,\wordCounter_reg[4]_i_1_n_6 ,\wordCounter_reg[4]_i_1_n_7 }),
        .S(wordCounter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[4]_i_1_n_6 ),
        .Q(wordCounter_reg[5]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[4]_i_1_n_5 ),
        .Q(wordCounter_reg[6]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[4]_i_1_n_4 ),
        .Q(wordCounter_reg[7]),
        .R(\wordCounter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[8]_i_1_n_7 ),
        .Q(wordCounter_reg[8]),
        .R(\wordCounter[0]_i_1_n_0 ));
  CARRY4 \wordCounter_reg[8]_i_1 
       (.CI(\wordCounter_reg[4]_i_1_n_0 ),
        .CO({\wordCounter_reg[8]_i_1_n_0 ,\wordCounter_reg[8]_i_1_n_1 ,\wordCounter_reg[8]_i_1_n_2 ,\wordCounter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wordCounter_reg[8]_i_1_n_4 ,\wordCounter_reg[8]_i_1_n_5 ,\wordCounter_reg[8]_i_1_n_6 ,\wordCounter_reg[8]_i_1_n_7 }),
        .S(wordCounter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wordCounter_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\wordCounter[0]_i_2_n_0 ),
        .D(\wordCounter_reg[8]_i_1_n_6 ),
        .Q(wordCounter_reg[9]),
        .R(\wordCounter[0]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0
   (m00_axis_tdata,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_aresetn,
    data_from_ram,
    m00_axis_aclk,
    en_AXIclk);
  output [31:0]m00_axis_tdata;
  output m00_axis_tlast;
  output m00_axis_tvalid;
  input m00_axis_aresetn;
  input [31:0]data_from_ram;
  input m00_axis_aclk;
  input en_AXIclk;

  wire [31:0]data_from_ram;
  wire en_AXIclk;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS samplGeneraror_v1_0_M00_AXIS_inst
       (.data_from_ram(data_from_ram),
        .en_AXIclk(en_AXIclk),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
