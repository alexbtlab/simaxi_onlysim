`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.04.2020 09:54:36
// Design Name: 
// Module Name: tb_sampleGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_sampleGenerator();
reg         Clk;
reg         ResetN;
wire   [31:0]     M00_AXIS_tdata;
reg              M00_AXIS_tready;
wire              M00_AXIS_tlast;
wire   [3:0]      M00_AXIS_tstrb;
wire              M00_AXIS_tvalid;
wire    [31:0]     data_out_ram_in_axi;
wire    [31:0]     adr_bram;
wire    [31:0]          data_out_adc_in_ram;
wire              clk_100Mhz_AXI_send;
wire              clk_5MHzWR_100MhzRD;
wire              write;

initial begin
    Clk = 0;
    forever #5 Clk = ~Clk;
end
initial begin
    M00_AXIS_tready = 0;
    #100 M00_AXIS_tready = 1;
end
initial begin
    ResetN = 0;
    #1000 ResetN = 1;
    #1000 ResetN = 0;
    #1000 ResetN = 1;
end

design_1_wrapper DUT
   (
    .M00_AXIS_tdata(M00_AXIS_tdata),
    .M00_AXIS_tlast(M00_AXIS_tlast),
    .M00_AXIS_tready(M00_AXIS_tready),
    .M00_AXIS_tstrb(M00_AXIS_tstrb),
    .M00_AXIS_tvalid(M00_AXIS_tvalid),
    .adr_bram(adr_bram),
    .clk_100Mhz_AXI_send(clk_100Mhz_AXI_send),
    .clk_5MHzWR_100MhzRD(clk_5MHzWR_100MhzRD),
    .data_out_adc_in_ram(data_out_adc_in_ram),
    .data_out_ram_in_axi(data_out_ram_in_axi),
    
    .m00_axis_aclk(Clk),
    .m00_axis_aresetn(ResetN),
    .write(write)
    );

endmodule
