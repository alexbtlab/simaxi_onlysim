
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7a100tfgg484-1
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set M00_AXIS [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS ]


  # Create ports
  set adr_bram [ create_bd_port -dir O -from 31 -to 0 adr_bram ]
  set clk_100Mhz_AXI_send [ create_bd_port -dir O clk_100Mhz_AXI_send ]
  set clk_5MHzWR_100MhzRD [ create_bd_port -dir O clk_5MHzWR_100MhzRD ]
  set clk_read_ram_0 [ create_bd_port -dir O clk_read_ram_0 ]
  set data_out_adc_in_ram [ create_bd_port -dir O -from 31 -to 0 data_out_adc_in_ram ]
  set data_out_ram_in_axi [ create_bd_port -dir O -from 31 -to 0 data_out_ram_in_axi ]
  set m00_axis_aclk [ create_bd_port -dir I -type clk m00_axis_aclk ]
  set m00_axis_aresetn [ create_bd_port -dir I -type rst m00_axis_aresetn ]
  set write [ create_bd_port -dir O write ]

  # Create instance: ADC_imitation_0, and set properties
  set ADC_imitation_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ADC_imitation:1.0 ADC_imitation_0 ]

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKOUT1_JITTER {290.478} \
   CONFIG.CLKOUT1_PHASE_ERROR {133.882} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {10.000} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {15.625} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {78.125} \
   CONFIG.MMCM_DIVCLK_DIVIDE {2} \
   CONFIG.PRIM_SOURCE {No_buffer} \
   CONFIG.USE_LOCKED {false} \
   CONFIG.USE_RESET {false} \
 ] $clk_wiz_0

  # Create instance: clk_wiz_1, and set properties
  set clk_wiz_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_1 ]
  set_property -dict [ list \
   CONFIG.CLKIN1_JITTER_PS {100.0} \
   CONFIG.CLKIN1_UI_JITTER {0.0010} \
   CONFIG.CLKOUT1_JITTER {921.673} \
   CONFIG.CLKOUT1_PHASE_ERROR {919.522} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {5.000} \
   CONFIG.CLKOUT1_USED {true} \
   CONFIG.CLK_OUT1_PORT {clk_out1_5MHz} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {64.000} \
   CONFIG.MMCM_CLKIN1_PERIOD {100.000} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {128.000} \
   CONFIG.MMCM_REF_JITTER1 {0.001} \
   CONFIG.PRIM_IN_FREQ {10.000} \
   CONFIG.USE_LOCKED {false} \
   CONFIG.USE_RESET {false} \
 ] $clk_wiz_1

  # Create instance: ram_0, and set properties
  set ram_0 [ create_bd_cell -type ip -vlnv bt.local:user:ram:1.0 ram_0 ]

  # Create instance: sampleGenerator_0, and set properties
  set sampleGenerator_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:sampleGenerator:1.0 sampleGenerator_0 ]

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {255} \
   CONFIG.CONST_WIDTH {16} \
 ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net sampleGenerator_0_M00_AXIS [get_bd_intf_ports M00_AXIS] [get_bd_intf_pins sampleGenerator_0/M00_AXIS]

  # Create port connections
  connect_bd_net -net ADC_imitation_0_adr_bram [get_bd_ports adr_bram] [get_bd_pins ADC_imitation_0/adr_bram] [get_bd_pins ram_0/i_addr]
  connect_bd_net -net ADC_imitation_0_clk_100Mhz_AXI_send [get_bd_ports clk_100Mhz_AXI_send] [get_bd_pins ADC_imitation_0/clk_100Mhz_AXI_send] [get_bd_pins sampleGenerator_0/m00_axis_aclk]
  connect_bd_net -net ADC_imitation_0_clk_5MHzWR_100MhzRD [get_bd_ports clk_5MHzWR_100MhzRD] [get_bd_pins ADC_imitation_0/clk_5MHzWR_100MhzRD] [get_bd_pins ram_0/i_clk]
  connect_bd_net -net ADC_imitation_0_data [get_bd_ports data_out_adc_in_ram] [get_bd_pins ADC_imitation_0/data] [get_bd_pins ram_0/i_data]
  connect_bd_net -net ADC_imitation_0_write [get_bd_ports clk_read_ram_0] [get_bd_ports write] [get_bd_pins ADC_imitation_0/write] [get_bd_pins ram_0/i_write] [get_bd_pins sampleGenerator_0/en_AXIclk]
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins clk_wiz_1/clk_in1]
  connect_bd_net -net clk_wiz_1_clk_out1_5MHz [get_bd_pins ADC_imitation_0/clk_5MHz] [get_bd_pins clk_wiz_1/clk_out1_5MHz]
  connect_bd_net -net m00_axis_aclk_0_1 [get_bd_ports m00_axis_aclk] [get_bd_pins ADC_imitation_0/clk_100MHz] [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins sampleGenerator_0/main_clk_100MHz]
  connect_bd_net -net m00_axis_aresetn_0_1 [get_bd_ports m00_axis_aresetn] [get_bd_pins ADC_imitation_0/s00_axi_aresetn] [get_bd_pins sampleGenerator_0/m00_axis_aresetn]
  connect_bd_net -net ram_0_o_data [get_bd_ports data_out_ram_in_axi] [get_bd_pins ram_0/o_data] [get_bd_pins sampleGenerator_0/data_from_ram]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins ADC_imitation_0/FrameSize] [get_bd_pins sampleGenerator_0/FrameSize] [get_bd_pins xlconstant_0/dout]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


