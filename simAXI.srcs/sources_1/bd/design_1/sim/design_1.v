//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Mon Jun  8 00:52:04 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=6,numReposBlks=6,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_mb_cnt=1,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (M00_AXIS_tdata,
    M00_AXIS_tlast,
    M00_AXIS_tready,
    M00_AXIS_tstrb,
    M00_AXIS_tvalid,
    adr_bram,
    clk_100Mhz_AXI_send,
    clk_5MHzWR_100MhzRD,
    clk_read_ram_0,
    data_out_adc_in_ram,
    data_out_ram_in_axi,
    m00_axis_aclk,
    m00_axis_aresetn,
    write);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, CLK_DOMAIN design_1_m00_axis_aclk, FREQ_HZ 100000000, HAS_TKEEP 0, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 1, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.000, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) output [31:0]M00_AXIS_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output M00_AXIS_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) input M00_AXIS_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]M00_AXIS_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output M00_AXIS_tvalid;
  output [31:0]adr_bram;
  output clk_100Mhz_AXI_send;
  output clk_5MHzWR_100MhzRD;
  output clk_read_ram_0;
  output [31:0]data_out_adc_in_ram;
  output [31:0]data_out_ram_in_axi;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.M00_AXIS_ACLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.M00_AXIS_ACLK, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, CLK_DOMAIN design_1_m00_axis_aclk, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.M00_AXIS_ARESETN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.M00_AXIS_ARESETN, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input m00_axis_aresetn;
  output write;

  wire [31:0]ADC_imitation_0_adr_bram;
  wire ADC_imitation_0_clk_100Mhz_AXI_send;
  wire ADC_imitation_0_clk_5MHzWR_100MhzRD;
  wire [31:0]ADC_imitation_0_data;
  wire ADC_imitation_0_write;
  wire clk_wiz_0_clk_out1;
  wire clk_wiz_1_clk_out1_5MHz;
  wire m00_axis_aclk_0_1;
  wire m00_axis_aresetn_0_1;
  wire [31:0]ram_0_o_data;
  wire [31:0]sampleGenerator_0_M00_AXIS_TDATA;
  wire sampleGenerator_0_M00_AXIS_TLAST;
  wire sampleGenerator_0_M00_AXIS_TREADY;
  wire [3:0]sampleGenerator_0_M00_AXIS_TSTRB;
  wire sampleGenerator_0_M00_AXIS_TVALID;
  wire [15:0]xlconstant_0_dout;

  assign M00_AXIS_tdata[31:0] = sampleGenerator_0_M00_AXIS_TDATA;
  assign M00_AXIS_tlast = sampleGenerator_0_M00_AXIS_TLAST;
  assign M00_AXIS_tstrb[3:0] = sampleGenerator_0_M00_AXIS_TSTRB;
  assign M00_AXIS_tvalid = sampleGenerator_0_M00_AXIS_TVALID;
  assign adr_bram[31:0] = ADC_imitation_0_adr_bram;
  assign clk_100Mhz_AXI_send = ADC_imitation_0_clk_100Mhz_AXI_send;
  assign clk_5MHzWR_100MhzRD = ADC_imitation_0_clk_5MHzWR_100MhzRD;
  assign clk_read_ram_0 = ADC_imitation_0_write;
  assign data_out_adc_in_ram[31:0] = ADC_imitation_0_data;
  assign data_out_ram_in_axi[31:0] = ram_0_o_data;
  assign m00_axis_aclk_0_1 = m00_axis_aclk;
  assign m00_axis_aresetn_0_1 = m00_axis_aresetn;
  assign sampleGenerator_0_M00_AXIS_TREADY = M00_AXIS_tready;
  assign write = ADC_imitation_0_write;
  design_1_ADC_imitation_0_0 ADC_imitation_0
       (.FrameSize(xlconstant_0_dout),
        .adr_bram(ADC_imitation_0_adr_bram),
        .adr_read_ram({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clk_100MHz(m00_axis_aclk_0_1),
        .clk_100Mhz_AXI_send(ADC_imitation_0_clk_100Mhz_AXI_send),
        .clk_5MHz(clk_wiz_1_clk_out1_5MHz),
        .clk_5MHzWR_100MhzRD(ADC_imitation_0_clk_5MHzWR_100MhzRD),
        .data(ADC_imitation_0_data),
        .last_word_transmit(1'b0),
        .s00_axi_aresetn(m00_axis_aresetn_0_1),
        .write(ADC_imitation_0_write));
  design_1_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(m00_axis_aclk_0_1),
        .clk_out1(clk_wiz_0_clk_out1));
  design_1_clk_wiz_1_0 clk_wiz_1
       (.clk_in1(clk_wiz_0_clk_out1),
        .clk_out1_5MHz(clk_wiz_1_clk_out1_5MHz));
  design_1_ram_0_0 ram_0
       (.i_addr(ADC_imitation_0_adr_bram[15:0]),
        .i_clk(ADC_imitation_0_clk_5MHzWR_100MhzRD),
        .i_data(ADC_imitation_0_data),
        .i_write(ADC_imitation_0_write),
        .o_data(ram_0_o_data));
  design_1_sampleGenerator_0_0 sampleGenerator_0
       (.FrameSize(xlconstant_0_dout),
        .data_from_ram(ram_0_o_data),
        .en_AXIclk(ADC_imitation_0_write),
        .m00_axis_aclk(ADC_imitation_0_clk_100Mhz_AXI_send),
        .m00_axis_aresetn(m00_axis_aresetn_0_1),
        .m00_axis_tdata(sampleGenerator_0_M00_AXIS_TDATA),
        .m00_axis_tlast(sampleGenerator_0_M00_AXIS_TLAST),
        .m00_axis_tready(sampleGenerator_0_M00_AXIS_TREADY),
        .m00_axis_tstrb(sampleGenerator_0_M00_AXIS_TSTRB),
        .m00_axis_tvalid(sampleGenerator_0_M00_AXIS_TVALID),
        .main_clk_100MHz(m00_axis_aclk_0_1));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule
