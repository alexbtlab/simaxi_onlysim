//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Thu Jun  4 23:11:01 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (M00_AXIS_tdata,
    M00_AXIS_tlast,
    M00_AXIS_tready,
    M00_AXIS_tstrb,
    M00_AXIS_tvalid,
    adr_bram,
    clk_100Mhz_AXI_send,
    clk_5MHzWR_100MhzRD,
    data_out_adc_in_ram,
    data_out_ram_in_axi,
    m00_axis_aclk,
    m00_axis_aresetn,
    write);
  output [31:0]M00_AXIS_tdata;
  output M00_AXIS_tlast;
  input M00_AXIS_tready;
  output [3:0]M00_AXIS_tstrb;
  output M00_AXIS_tvalid;
  output [31:0]adr_bram;
  output clk_100Mhz_AXI_send;
  output clk_5MHzWR_100MhzRD;
  output [31:0]data_out_adc_in_ram;
  output [31:0]data_out_ram_in_axi;
  input m00_axis_aclk;
  input m00_axis_aresetn;
  output write;

  wire [31:0]M00_AXIS_tdata;
  wire M00_AXIS_tlast;
  wire M00_AXIS_tready;
  wire [3:0]M00_AXIS_tstrb;
  wire M00_AXIS_tvalid;
  wire [31:0]adr_bram;
  wire clk_100Mhz_AXI_send;
  wire clk_5MHzWR_100MhzRD;
  wire [31:0]data_out_adc_in_ram;
  wire [31:0]data_out_ram_in_axi;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire write;

  design_1 design_1_i
       (.M00_AXIS_tdata(M00_AXIS_tdata),
        .M00_AXIS_tlast(M00_AXIS_tlast),
        .M00_AXIS_tready(M00_AXIS_tready),
        .M00_AXIS_tstrb(M00_AXIS_tstrb),
        .M00_AXIS_tvalid(M00_AXIS_tvalid),
        .adr_bram(adr_bram),
        .clk_100Mhz_AXI_send(clk_100Mhz_AXI_send),
        .clk_5MHzWR_100MhzRD(clk_5MHzWR_100MhzRD),
        .data_out_adc_in_ram(data_out_adc_in_ram),
        .data_out_ram_in_axi(data_out_ram_in_axi),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .write(write));
endmodule
