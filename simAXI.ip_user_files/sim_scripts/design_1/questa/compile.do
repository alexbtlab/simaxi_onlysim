vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xil_defaultlib
vlib questa_lib/msim/xpm
vlib questa_lib/msim/xlconstant_v1_1_6

vmap xil_defaultlib questa_lib/msim/xil_defaultlib
vmap xpm questa_lib/msim/xpm
vmap xlconstant_v1_1_6 questa_lib/msim/xlconstant_v1_1_6

vlog -work xil_defaultlib -64 -sv "+incdir+../../../../simAXI.srcs/sources_1/bd/design_1/ipshared/c923" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -64 -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 "+incdir+../../../../simAXI.srcs/sources_1/bd/design_1/ipshared/c923" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0.v" \
"../../../bd/design_1/ipshared/4960/hdl/ADC_imitation_v1_0.v" \
"../../../bd/design_1/ip/design_1_ADC_imitation_0_0/sim/design_1_ADC_imitation_0_0.v" \
"../../../bd/design_1/ipshared/1e87/hdl/ram_v1_0.v" \
"../../../bd/design_1/ip/design_1_ram_0_0/sim/design_1_ram_0_0.v" \
"../../../bd/design_1/ipshared/7330/hdl/sampleGenerator_v1_0_M00_AXIS.v" \
"../../../bd/design_1/ipshared/7330/hdl/sampleGenerator_v1_0.v" \
"../../../bd/design_1/ip/design_1_sampleGenerator_0_0/sim/design_1_sampleGenerator_0_0.v" \

vlog -work xlconstant_v1_1_6 -64 "+incdir+../../../../simAXI.srcs/sources_1/bd/design_1/ipshared/c923" \
"../../../../simAXI.srcs/sources_1/bd/design_1/ipshared/66e7/hdl/xlconstant_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../simAXI.srcs/sources_1/bd/design_1/ipshared/c923" \
"../../../bd/design_1/ip/design_1_xlconstant_0_0/sim/design_1_xlconstant_0_0.v" \
"../../../bd/design_1/sim/design_1.v" \

vlog -work xil_defaultlib \
"glbl.v"

